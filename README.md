Setup Up environment  with requirements.txt or run pipenv install 

There are three spiders in this scrapy project.
books.py
booksMongo
selenium.py

Books.py scrapes http://books.toscrape.com/ by scraping books of each page of a book category. Book details such as title, price, image, rating and description are also gathered.
To run, type in terminal 
 scrapy crawl books -o books.json 
 #this will be stored in a file name books.json 

booksMongo works exactly as books.py but rather saves the output in a mongo db database.To run, type in terminal 
 scrapy crawl booksMongo
#ensure the DB is setup with the correct parameters in the close function 

Selenium.py shows how you can scrapy with selenium to extract  data. It is required to use a chrome driver fro selenium 
This can be run with 
 scrapy crawl selenium